TABLEAU_COUNT=50
RESULTS_DIR=results
TIMES_FILE=$RESULTS_DIR/times.csv
SKIP_CPU=0

rm -rf $RESULTS_DIR
mkdir $RESULTS_DIR

for VAR_COUNT in 500 1000 2000 4000 6000 8000 10000 12000
do
  echo "Running benchmark for $TABLEAU_COUNT problems with $VAR_COUNT variables..."
  FILENAME="$RESULTS_DIR/benchmark-$TABLEAU_COUNT-$VAR_COUNT.txt"
  echo "Saving results in $FILENAME"
  ./bin/benchmark $TABLEAU_COUNT $VAR_COUNT $SKIP_CPU $TIMES_FILE > $FILENAME
  echo ""
done
