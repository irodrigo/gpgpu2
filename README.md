# Simplex Tableau in GPU

Basic GPU implementation of the [Simplex Tableau algorithm](https://en.wikipedia.org/wiki/Simplex_algorithm) for solving the linear programming problem.
There are CPU-only implementations of the algorithm in C and Python too, used mainly for benchmarking purposes.

The GPU implementation uses [nVIDIA CUDA technology](https://developer.nvidia.com/cuda-zone).

This project is part of an [advanced GPGPU collage course](http://www.fing.edu.uy/inco/cursos/gpgpu/taller/).


## CPU implementation

The CPU-only implementation in C can be found in the `src/tableau.h` and `src/tableau.c` files.

All methods in the header file are properly documented.


## GPU implementation

The parallel GPU implementation in CUDA-C can be found in the `tableau.cuh` and `tableau.cu` files.


## Python implementation

The Python implementation is located in the `python/tableau.py` file, with unit tests in `pyton/tests.py` and benchmarking utilities in `python/benchmark.py`.

All modules are properly documented.

This was the initial implementation, is not optimized at all and it's several orders of magnitude slower than it's C counterpart.


## Docs

The project docs are in the `informe.md` file (rendered in the `informe.pdf` file), in Spanish for the course.
All the development process and decisions are explained there, as well as the research.

This articles are used as a reference for the whole project:
- [V. Boyer, D. El Baz, Recent Advances on GPU Computing in Operations Research, IEEE 27th International Symposium on Parallel & Distributed Processing Workshops and PhD Forum, 2013](https://drive.google.com/open?id=0BxQYEalXLJXbT2NJRnlGU3VsdE0)
- [M. Lalami, V. Boyer, and D. El Baz, “Efficient implementation of the simplex method on a CPU-GPU system,” in 25th IEEE International Parallel and Distributed Processing Symposium, Workshops and Phd Forum (IPDPSW), Workshop PCO’11, may 2011, pp. 1999 –2006.](https://drive.google.com/open?id=0BxQYEalXLJXbT2NJRnlGU3VsdE0)


## Build and run

The `clean.sh` and `build.sh` scripts can be used to clean and build the C and CUDA-C code.
The `test.sh` and `benchmark.sh` scripts can be used to test and benchmark the C and CUDA-C code.
