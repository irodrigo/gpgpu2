/**
 * Execution timing utilities.
 */

#ifndef TIMING_H
#define TIMING_H

#include <stdio.h>
#include <stdlib.h>

/**
 * Start the stepwatch for measuring execution time.
 */
void clockStart();

/**
 * Stop the previously started stepwatch and return the elapsed time in seconds.
 */
double clockStop();


#endif
