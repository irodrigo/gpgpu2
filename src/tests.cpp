/**
 * Test suite for the simplex tableau implementation.
 */

#include "testing.h"
#include "tableauGPU.cuh"
#include "ansicolor.h"


#define VAR_COUNT 1024


void testBuildTableau(){
  Tableau tableau = defaultTableau();
  float expectedTableau[32] = {
    1.0f, -1.0f, -3.0f, -5.0f, 0.0f, 0.0f, 0.0f,   0.0f,
    0.0f,  1.0f,  0.0f,  2.0f, 1.0f, 0.0f, 0.0f, 120.0f,
    0.0f,  3.0f,  1.0f,  0.0f, 0.0f, 1.0f, 0.0f,  90.0f,
    0.0f,  0.0f,  2.0f,  1.0f, 0.0f, 0.0f, 1.0f, 110.0f
  };
  Tableau expectedFullTableau;
  expectedFullTableau.tableau = expectedTableau;
  expectedFullTableau.rows = 4;
  expectedFullTableau.columns = 8;
  expectedFullTableau.varCount = 3;
  expectedFullTableau.constraintCount = 3;
  assertAlmostEqualTableau(tableau, expectedFullTableau);
  freeTableau(tableau);
  printf("testBuildTableau passed\n");
}


void testIsSolved(){
  Tableau tableau = defaultTableau();
  assert(!_isSolved(tableau));
  float *solution = solveTableau(tableau);
  assert(_isSolved(tableau));
  freeTableau(tableau);
  free(solution);
  printf("testIsSolved passed\n");
}


void testPivotColumn(){
  Tableau tableau = defaultTableau();
  assert(_pivotColumn(tableau) == 3);
  float *solution = solveTableau(tableau);
  assert(_pivotColumn(tableau) == -1);
  freeTableau(tableau);
  free(solution);
  printf("testPivotColumn passed\n");
}


void testPivotRow(){
  Tableau tableau = defaultTableau();
  assert(_pivotRow(tableau, 3) == 1);
  freeTableau(tableau);
  printf("testPivotRow passed\n");
}


void testPivot(){
  Tableau tableau = defaultTableau();

  _pivot(tableau, 1, 3);

  float expectedTableau[32] = {
    1.0f,  1.5f, -3.0f,  0.0f,  2.5f, 0.0f, 0.0f, 300.0f,
    0.0f,  0.5f,  0.0f,  1.0f,  0.5f, 0.0f, 0.0f,  60.0f,
    0.0f,  3.0f,  1.0f,  0.0f,  0.0f, 1.0f, 0.0f,  90.0f,
    0.0f, -0.5f,  2.0f,  0.0f, -0.5f, 0.0f, 1.0f,  50.0f
  };
  Tableau expectedFullTableau;
  expectedFullTableau.tableau = expectedTableau;
  expectedFullTableau.rows = 4;
  expectedFullTableau.columns = 8;
  expectedFullTableau.varCount = 3;
  expectedFullTableau.constraintCount = 3;

  assertAlmostEqualTableau(tableau, expectedFullTableau);
  assert(!_isSolved(tableau));
  assert(_pivotColumn(tableau) == 2);
  assert(_pivotRow(tableau, 2) == 3);

  _pivot(tableau, 3, 2);

  float expectedTableau2[32] = {
    1.0f,  0.75f,  0.0f,  0.0f,  1.75f, 0.0f,  1.5f, 375.0f,
    0.0f,   0.5f,  0.0f,  1.0f,   0.5f, 0.0f,  0.0f,  60.0f,
    0.0f,  3.25f,  0.0f,  0.0f,  0.25f, 1.0f, -0.5f,  65.0f,
    0.0f, -0.25f,  1.0f,  0.0f, -0.25f, 0.0f,  0.5f,  25.0f
  };
  expectedFullTableau.tableau = expectedTableau2;

  assertAlmostEqualTableau(tableau, expectedFullTableau);
  assert(_isSolved(tableau));

  freeTableau(tableau);

  printf("testPivot passed\n");
}


void testActiveVariableRow(){
  Tableau tableau = defaultTableau();

  assert(_activeVariableRow(tableau, 0) ==  0);
  assert(_activeVariableRow(tableau, 1) == -1);
  assert(_activeVariableRow(tableau, 2) == -1);
  assert(_activeVariableRow(tableau, 3) == -1);
  assert(_activeVariableRow(tableau, 4) ==  1);
  assert(_activeVariableRow(tableau, 5) ==  2);
  assert(_activeVariableRow(tableau, 6) ==  3);
  assert(_activeVariableRow(tableau, 7) == -1);

  freeTableau(tableau);

  printf("testActiveVariableRow passed\n");
}


void testSolve(){
  Tableau tableau = defaultTableau();
  float *solution = solveTableau(tableau);
	assert(solution != NULL);
	float optimal[3] = {0.0f, 25.0f, 60.0f};
  assertAlmostEqualArray(solution, optimal, 3);
	freeTableau(tableau);
	free(solution);
  printf("testSolve passed\n");
}


void testSolveTableauInGPU(){
  Tableau tableau = defaultTableau();
  float *solution = solveTableauInGPU(tableau);
  assert(solution != NULL);
  float optimal[3] = {0.0f, 25.0f, 60.0f};
  assertAlmostEqualArray(solution, optimal, 3);
  freeTableau(tableau);
  free(solution);
  printf("testSolve passed\n");
}


void testCompareCPUandGPUsolutions(){

  Tableau tableauCPU = randomTableau(VAR_COUNT);
  Tableau tableauGPU = deepCopyTableau(tableauCPU);
  assertAlmostEqualTableau(tableauCPU, tableauGPU); // sanity check

  float *solutionCPU = solveTableau(tableauCPU);
	assert(solutionCPU != NULL);

  float *solutionGPU = solveTableauInGPU(tableauGPU);
  assert(solutionGPU != NULL);

  assertAlmostEqualArray(solutionCPU, solutionGPU, tableauCPU.varCount);

  freeTableau(tableauCPU);
  freeTableau(tableauGPU);
  free(solutionCPU);
  free(solutionGPU);

  printf("testCompareCPUandGPUsolutions passed\n");
}


int main(int argc, char const *argv[]){
  printf("Running tests...\n");
  testBuildTableau();
  testIsSolved();
  testPivotColumn();
  testPivotRow();
  testPivot();
  testActiveVariableRow();
  testSolve();
  testSolveTableauInGPU();
  testCompareCPUandGPUsolutions();
  printf(ANSI_COLOR_GREEN "All tests passed!\n" ANSI_COLOR_RESET);
  return 0;
}
