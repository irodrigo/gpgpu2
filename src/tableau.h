/**
 * Solve the linear programming problem with the Simplex Tableau method.
 * This is a naive CPU implementation of the algorithm.
 */

#ifndef TABLEAU_H
#define TABLEAU_H

#include <stdbool.h>


/**
 * Helper macro for converting between matrix and array indexes.
 * Takes the original row, column and columnCount of the matrix and returns the
 * index of the corresponding element in the flat array representation of the matrix.
 * Matrices are stored as flat arrays for GPU interoperability.
 */
#define INDEX(row, column, columnCount) ((row)*(columnCount) + (column))


/**
 * Basic struct representing a tableau for the simplex method.
 */
typedef struct Tableau {
	float *tableau; // the full tableau, including slack variables and everything
  // the tableau is stored as a flat array for GPU compatibility.
	int rows; // the number of rows in the full tableau
	int columns; // the number of columns in the full tableau
	int varCount; // the number of variables in the target function
	int constraintCount; // the number of constraints in the problem
} Tableau;


/**
 * Build a random tableau with the given number of decision variables.
 */
Tableau randomTableau(int varCount);


/**
 * Build a tableau with the given target function, constraints and values.
 * It adds the slack variables and formats the data for the rest of the algorithms.
 */
Tableau buildTableau(float *target, float *constraints, float *values, int varCount, int constraintCount);


/***
 * Perform a deep copy of the given tableau.
 */
Tableau deepCopyTableau(Tableau tableau);


/**
 * Free the alocated memory for the given tableau.
 * Assumes the given tableau was created by `buildTableau`.
 */
void freeTableau(Tableau tableau);


/**
 * Solve the given tableau using the simplex algorithm on the CPU.
 * Returns the optimal solution, or NULL if the problem is unbounded.
 */
float* solveTableau(Tableau tableau);


/**
 * Check if the given tableau is solved.
 */
bool _isSolved(Tableau tableau);


/**
 * Return the current solution for the tableau.
 * If the tableau is solved, then this is the optimal solution.
 */
float* _solution(Tableau tableau);


/**
 * Perform a pivot operation on the given tableau using the given pivot row and
 * pivot column.
 * A pivot operation sets all the values of the pivot column to zero except
 * for the pivot itself, by performing simple row operations.
 */
void _pivot(Tableau tableau, int pivotRow, int pivotColumn);


/**
 * Calculate the pivot column of the given tableau.
 * The pivot column is the column where the target function row has the
 * negative value with the largest magnitude.
 * If all the values of the target function row are non-negative, then the
 * tableau is solved, and -1 is returned.
 */
int _pivotColumn(Tableau tableau);


/**
 * Calculate the pivot row for the current tableau, given the pivot column.
 * The pivot row is the row corresponding to the minumum test ratio.
 * Each test ratio is calculated as a/b where a is the value of the
 * right-most column and b is a positve value in the given pivot column,
 * excluding the row corresponding to the target function.
 */
int _pivotRow(Tableau tableau, int pivotColumn);


/**
 * Get the row for the active variable in the given column if any.
 * A variable is active if it's the only non-zero variable in it's column.
 * Returns -1 if there's no active variable in the given column.
 */
int _activeVariableRow(Tableau tableau, int column);


#endif // TABLEAU_H
