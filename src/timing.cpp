#include "timing.h"
#include <sys/time.h>


struct timeval startTime;


void clockStart(){
	gettimeofday(&startTime, NULL);
}


double clockStop(){
	struct timeval endTime;
	gettimeofday(&endTime, NULL);
	double elapsedTime = endTime.tv_sec - startTime.tv_sec;
	elapsedTime += (double)(endTime.tv_usec - startTime.tv_usec) / 1000000.0;
	return elapsedTime;
}
