/**
 * Benchmarks for the simplex tableau implementation.
 * Generate and solve some random problems, and time the efforts.
 */

#include <stdlib.h>
#include <stdio.h>
#include <assert.h>
#include <math.h>
#include <float.h>

#include "tableau.h"
#include "timing.h"
#include "tableauGPU.cuh"
#include "testing.h"
#include "ansicolor.h"


// default parameters
const int DEFAULT_TABLEAU_COUNT = 50;
const int DEFAULT_VAR_COUNT = 3000;


/**
 * Main function.
 * Create random linear programming problems and solve them using bothe CPU and
 * GPU implementations of the Simplex algorithm.
 * The number of tableaus to create as well as the number of variables in each
 * can be passed as arguments, else default values are used.
 */
int main(int argc, char const *argv[])
{
	// get arguments
	if(argc > 5){
		printf(ANSI_COLOR_RED "Usage: tableau [tableau_count] [var_count] [skip_CPU] [times_file]\n" ANSI_COLOR_RESET);
		printf("tableau_count is the number of tandom tableaus to solve\n");
		printf("var_count is the number of variables in each tableau\n");
		printf("skip_CPU is a 0 or 1 flag to skip the reference CPU algorithms and run only the GPU ones\n");
		printf("times_file is an optional filename to save (append) average execution times in CSV format\n");
		exit(1);
	}

	const int tableauCount = (argc > 1) ? atoi(argv[1]) : DEFAULT_TABLEAU_COUNT;
	const int varCount = (argc > 2) ? atoi(argv[2]) : DEFAULT_VAR_COUNT;
	const bool skipCPU = (argc > 3) ? atoi(argv[3]) : false;
	const char *timesFilename = (argc > 4) ? argv[4] : NULL;

	printf(ANSI_COLOR_GREEN "Running simplex algorithms on %i random tableaus, with %i decission variables each\n" ANSI_COLOR_RESET, tableauCount, varCount);
	if(skipCPU){
		printf("Skipping CPU algorithms\n");
	}

	// run algorithms
	double totalTimeCPU = 0.0f;
	double maxTimeCPU = 0.0f;
	double minTimeCPU = DBL_MAX;
	double totalTimeGPU = 0.0f;
	double maxTimeGPU = 0.0f;
	double minTimeGPU = DBL_MAX;
	int unboundedProblems = 0;
	int percentDone = 0;

	for (int t = 0; t < tableauCount; t++){
		// generate a random tambleau
		Tableau tableau = randomTableau(varCount);

		// solve the tableau using the naive CPU implementation
		float *solutionCPU;
		Tableau tableauCPU;
		if (!skipCPU) {
			tableauCPU = deepCopyTableau(tableau);
			clockStart();
			solutionCPU = solveTableau(tableauCPU);
			double solveTimeCPU = clockStop();

			totalTimeCPU += solveTimeCPU;
			maxTimeCPU = fmax(maxTimeCPU, solveTimeCPU);
			minTimeCPU = fmin(minTimeCPU, solveTimeCPU);
		}

		// solve the tableau using the optimized GPU implementation
		pinTableau(tableau);
		clockStart();
		float *solutionGPU = solveTableauInGPU(tableau);
		double solveTimeGPU = clockStop();
		unpinTableau(tableau);

		if (solutionGPU == NULL){
			unboundedProblems++;
		}

		totalTimeGPU += solveTimeGPU;
		maxTimeGPU = fmax(maxTimeGPU, solveTimeGPU);
		minTimeGPU = fmin(minTimeGPU, solveTimeGPU);

		// check that the solutions are equal
		if (!skipCPU) {
			assertAlmostEqualArray(solutionCPU, solutionGPU, tableau.varCount);
		}

		// clean up
		freeTableau(tableau);
		free(solutionGPU);
		if (!skipCPU) {
			freeTableau(tableauCPU);
			free(solutionCPU);
		}

		// show progress
		if (tableauCount >= 10 && t % (tableauCount/10) == 0){
			printf("%i%% done\n", percentDone);
			percentDone += 10;
		}
	}

	// calculate averages
	double averageCPU = totalTimeCPU / tableauCount;
	double averageGPU = totalTimeGPU / tableauCount;

	// save results in CSV
	if(timesFilename){
		printf("Appending average execution times to %s in CSV format (#VARS,AVG_CPU_TIME,AVG_GPU_TIME)\n", timesFilename);
		FILE *timesFile = fopen(timesFilename, "a");
		fprintf(timesFile, "%i,%lf,%lf\n", varCount, averageCPU, averageGPU);
		fclose(timesFile);
	}

	// show results
	printf(ANSI_COLOR_GREEN "Solved %i random tableaus with %i decision variables each\n" ANSI_COLOR_RESET, tableauCount, varCount);
	printf("\n");
	printf("Found %i unbounded problems\n", unboundedProblems);
	printf("\n");

	if (!skipCPU) {
		printf("- Total time on CPU: %lf seconds\n", totalTimeCPU);
		printf("- Average time on CPU: %lf seconds\n", averageCPU);
		printf("- Max time on CPU: %lf seconds\n", maxTimeCPU);
		printf("- Min time on CPU: %lf seconds\n", minTimeCPU);
		printf("\n");
	}

	printf("- Total time on GPU: %lf seconds\n", totalTimeGPU);
	printf("- Average time on GPU: %lf seconds\n", averageGPU);
	printf("- Max time on GPU: %lf seconds\n", maxTimeGPU);
	printf("- Min time on GPU: %lf seconds\n", minTimeGPU);
	printf("\n");

	if (!skipCPU) {
		printf("Average speedup: %.02lf\n", totalTimeCPU / totalTimeGPU);
	}

	return 0;
}
