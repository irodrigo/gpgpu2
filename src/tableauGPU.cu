#include <cuda.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <float.h>

#include "tableauGPU.cuh"
#include "ansicolor.h"


#define DEBUG false


/***************************** DEVICE CODE ***********************************/


/**
 * Kernel for calculating the test ratios in GPU.
 */
__global__ void __testRatiosKernel(float *tableau, int rows, int columns, int pivotColumn, float *testRatios){

	int row = blockDim.x * blockIdx.x + threadIdx.x;

	if (row < rows && tableau[INDEX(row, pivotColumn, columns)] > 0.0f) {
		int lastColumnIndex = columns - 1;
		testRatios[row] = tableau[INDEX(row, lastColumnIndex, columns)] / tableau[INDEX(row, pivotColumn, columns)];
	}
}


/**
 * Kernel for updating the pivot row in GPU.
 */
__global__ void __updatePivotRowKernel(float *tableau, int columns, int pivotRow, float pivotValue){

	int col = blockDim.x * blockIdx.x + threadIdx.x;

	if (col < columns) {
		tableau[INDEX(pivotRow, col, columns)] /= pivotValue;
	}
}


/**
 * Kernel for copying the pivot column in GPU.
 */
__global__ void __copyPivotColumnKernel(float *tableau, int rows, int columns, int pivotColumn, float *pivotColumnInGPU){
	int row = blockDim.x * blockIdx.x + threadIdx.x;
	if (row < rows){
			pivotColumnInGPU[row] = tableau[INDEX(row, pivotColumn, columns)];
	}
}

/**
 * Kernel for performing a pivot operation on the given tableau, pivot column and pivot row.
 */
__global__ void __pivotOperationKernel(float *tableau, int rows, int columns, int pivotRow, int pivotColumn, float *pivotColumnInGPU){

	// calculate row and column
	int row = blockDim.y * blockIdx.y + threadIdx.y;
	int col = blockDim.x * blockIdx.x + threadIdx.x;

	// copy row value at pivot column to shared memory
	extern __shared__ float pivotColumnValues[];
	if(threadIdx.x == 0 && row < rows){
		pivotColumnValues[threadIdx.y] = pivotColumnInGPU[row];
	}
	__syncthreads();

	// check boundaries
	if (row < rows && col < columns && row != pivotRow){
		tableau[INDEX(row, col, columns)] -= pivotColumnValues[threadIdx.y] * tableau[INDEX(pivotRow, col, columns)];
	}
}


/***************************** HOST CODE ***********************************/


void checkCUDAErrors(const char *message){
	cudaError_t cudaError;
	cudaError = cudaGetLastError();
	if(cudaError != cudaSuccess){
		printf(ANSI_COLOR_RED "%s\n" ANSI_COLOR_RESET, message);
		printf(ANSI_COLOR_RED "cudaGetLastError() returned %d: %s\n" ANSI_COLOR_RESET, cudaError, cudaGetErrorString(cudaError));
		exit(1);
	}
}


int _pivotColumnInCPU(float *firstRowInCPU, int columns){
	float minValue = 0.0f;
	int minValueIndex = -1;

	for (int i = 1; i < columns - 1; i++){
		float val = firstRowInCPU[i];
		if (val < minValue){
			minValue = val;
			minValueIndex = i;
		}
	}

	return minValueIndex;
}


void _calculateTestRatiosInGPU (float *tableauInGPU, int rows, int columns, int pivotColumn, float *testRatiosInGPU) {
	// launch the kernel
	int blockDim = 512;
	int gridDim = rows / blockDim + 1;

	__testRatiosKernel<<<gridDim, blockDim>>>(tableauInGPU, rows, columns, pivotColumn, testRatiosInGPU);

	if(DEBUG){
		cudaDeviceSynchronize();
		checkCUDAErrors("can't calculate test ratios");
	}
}


int _pivotRowInCPU(float *testRatiosInCPU, int rows){
	float minValue = FLT_MAX;
	int minValueIndex = -1;

	for (int row = 1; row < rows; row++){
		float val = testRatiosInCPU[row];
		if (val < minValue){
			minValue = val;
			minValueIndex = row;
		}
	}

	return minValueIndex;
}


void _updatePivotRowInGPU(float *tableauInGPU, int rows, int columns, int pivotRow, int pivotColumn){
	// copy pivot value from GPU to avoid calculating it in each thread
	float pivotValueInCPU = 0.0f;
	float *pivotValueInGPU = tableauInGPU + INDEX(pivotRow, pivotColumn, columns);
	cudaMemcpy(&pivotValueInCPU, pivotValueInGPU, sizeof(float), cudaMemcpyDeviceToHost);
	checkCUDAErrors("can't copy pivot value from GPU");

	// launch kernel
	int blockDim = 512;
	int gridDim = columns / blockDim + 1;

	__updatePivotRowKernel<<<gridDim, blockDim>>>(tableauInGPU, columns, pivotRow, pivotValueInCPU);

	if(DEBUG){
		cudaDeviceSynchronize();
		checkCUDAErrors("can't update pivot row in GPU");
	}
}


void _copyPivotColumnInGPU(float *tableauInGPU, int rows, int columns, int pivotColumn, float *pivotColumnInGPU){
	// launch kernel
	int blockDim = 512;
	int gridDim = rows / blockDim + 1;

	__copyPivotColumnKernel<<<gridDim, blockDim>>>(tableauInGPU, rows, columns, pivotColumn, pivotColumnInGPU);

	if(DEBUG){
		cudaDeviceSynchronize();
		checkCUDAErrors("can't copy pivot column in GPU");
	}
}



void _performPivotOperationsInGPU(float *tableauInGPU, int rows, int columns, int pivotRow, int pivotColumn, float *pivotColumnInGPU){
	int blockDimX = 32;
	int blockDimY = 16;
	dim3 blockDim(blockDimX, blockDimY);

	int gridDimX = ceilf((float)columns / (float)blockDimX);
	int gridDimY = ceilf((float)rows / (float)blockDimY);
	dim3 gridDim(gridDimX, gridDimY);

	size_t sharedMemorySize = blockDimY * sizeof(float);

	__pivotOperationKernel<<<gridDim, blockDim, sharedMemorySize>>>(tableauInGPU, rows, columns, pivotRow, pivotColumn, pivotColumnInGPU);

	if(DEBUG){
		cudaDeviceSynchronize();
		checkCUDAErrors("can't perform pivot operations in GPU");
	}
}


float *solveTableauInGPU(Tableau tableau){
	// create CUDA streams
	cudaStream_t stream1;
	cudaStreamCreate(&stream1);

	// copy data to GPU
	size_t tableauSize = tableau.rows * tableau.columns * sizeof(float);
	float *tableauGPU = NULL;
	cudaMalloc(&tableauGPU, tableauSize);
	cudaMemcpyAsync(tableauGPU, tableau.tableau, tableauSize, cudaMemcpyHostToDevice, stream1);
	checkCUDAErrors("can't copy tableau to GPU");

	// allocate memory for intermediate operations
	size_t rowSize = tableau.columns * sizeof(float);
	float *firstRowInCPU = NULL;
	cudaMallocHost(&firstRowInCPU, rowSize);

	size_t columnSize = tableau.rows * sizeof(float);
	float *testRatiosInGPU = NULL;
	cudaMalloc(&testRatiosInGPU, columnSize);

	float *testRatiosInCPU = NULL;
	cudaMallocHost(&testRatiosInCPU, columnSize);

	float *pivotColumnInGPU = NULL;
	cudaMalloc(&pivotColumnInGPU, columnSize);

	// solve tableau in GPU
	bool isSolved = false;
	cudaStreamSynchronize(stream1);
	while (!isSolved) {

		// calculate pivot column in CPU
		cudaMemcpy(firstRowInCPU, tableauGPU, rowSize, cudaMemcpyDeviceToHost);
		int pivotColumn = _pivotColumnInCPU(firstRowInCPU, tableau.columns);

		// check if the problem is solved already
		isSolved = (pivotColumn < 0);
		if (isSolved) {
			break;
		}

		// calculate the pivot row in CPU
		cudaMemset(testRatiosInGPU, FLT_MAX, columnSize);
		_calculateTestRatiosInGPU(tableauGPU, tableau.rows, tableau.columns, pivotColumn, testRatiosInGPU);
		cudaMemcpy(testRatiosInCPU, testRatiosInGPU, columnSize, cudaMemcpyDeviceToHost);
		int pivotRow = _pivotRowInCPU(testRatiosInCPU, tableau.rows);

		// check if the problem is unbounded
		if (pivotRow == -1){
			// the problem is unbounded
			break;
		}

		// perform the pivot operations in GPU
		_updatePivotRowInGPU(tableauGPU, tableau.rows, tableau.columns, pivotRow, pivotColumn);
		_copyPivotColumnInGPU(tableauGPU, tableau.rows, tableau.columns, pivotColumn, pivotColumnInGPU);
		_performPivotOperationsInGPU(tableauGPU, tableau.rows, tableau.columns, pivotRow, pivotColumn, pivotColumnInGPU);
	}

	// copy the updated tableau back to the CPU
	cudaMemcpyAsync(tableau.tableau, tableauGPU, tableauSize, cudaMemcpyDeviceToHost, stream1);

	// free resources
	cudaFreeHost(firstRowInCPU);
	cudaFreeHost(testRatiosInCPU);
	cudaFree(pivotColumnInGPU);
	cudaFree(testRatiosInGPU);

	cudaStreamSynchronize(stream1);
	cudaFree(tableauGPU);
	cudaStreamDestroy(stream1);

	// calculate the solution in CPU if the tableau was solved
	float *solution = NULL;
	if (isSolved) {
		solution = _solution(tableau);
	}

	return solution;
}


void pinTableau(Tableau tableau){
	size_t tableauSize = tableau.rows * tableau.columns * sizeof(float);
	cudaHostRegister(tableau.tableau, tableauSize, cudaHostRegisterMapped);
	checkCUDAErrors("can't pin tableau memory");
}


void unpinTableau(Tableau tableau){
	cudaHostUnregister(tableau.tableau);
}
