/**
 * Basic testing utilities.
 */

#ifndef TESTING_H
#define TESTING_H

#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>
#include <assert.h>
#include <math.h>

#include "tableau.h"


/**
 * The maximum difference between two floats to consider them almost equal.
 */
#define EPSILON 0.0005f


/**
 * Assert that two floats are almost equal.
 */
void assertAlmostEqual(float f1, float f2);


/**
 * Assert that two arrays of floats are almost equal.
 */
void assertAlmostEqualArray(float *a1, float *a2, int length);


/**
 * Assert that two matrices of floats are almost equal.
 * Matrices are represented as arrays for GPU interoperability.
 */
void assertAlmostEqualMatrix(float *m1, float *m2, int rows, int columns);


/**
 * Assert that two tableaus are almost equal.
 */
void assertAlmostEqualTableau(Tableau t1, Tableau t2);


/**
 * Create a very simple tableau with a known solution.
 * The optimal solution is [0.0f, 25.0f, 60.0f].
 */
Tableau defaultTableau();


/**
 * Print the given tableau to the standard output.
 */
void printTableau(Tableau tableau);


#endif
