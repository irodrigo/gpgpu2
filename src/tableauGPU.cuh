/**
 * Solve the linear programming problem with the Simplex Tableau method.
 * This is an optimized GPU implementation of the algorithm.
 */

#ifndef TABLEAUGPU_H
#define TABLEAUGPU_H


#include "tableau.h"

/**
 * Solve the given tableau on the GPU.
 * Involves copying data to the GPU memory, running the needed kernels,
 * and getting the solution back from the GPU memory.
 * Returns the optimal solution of NULL if the problem is unbounded.
 */
float* solveTableauInGPU(Tableau tableau);

void pinTableau(Tableau tableau);
void unpinTableau(Tableau tableau);

#endif
