#include "testing.h"


void assertAlmostEqual(float f1, float f2){
  bool equal = (fabsf(f1 - f2) < EPSILON);
  if(!equal){
    printf("%f != %f", f1, f2);
  }
  assert(equal);
}


void assertAlmostEqualArray(float *a1, float *a2, int length){
  for (int i = 0; i < length; i++){
    assertAlmostEqual(a1[i], a2[i]);
  }
}


void assertAlmostEqualMatrix(float *m1, float *m2, int rows, int columns){
  for (int r = 0; r < rows; r++){
    for (int c = 0; c < columns; c++){
      assertAlmostEqual(m1[INDEX(r, c, columns)], m2[INDEX(r, c, columns)]);
    }
  }
}


void assertAlmostEqualTableau(Tableau t1, Tableau t2){
  assert(t1.rows == t2.rows);
  assert(t1.columns == t2.columns);
  assert(t1.varCount == t2.varCount);
  assert(t1.constraintCount == t2.constraintCount);
  assertAlmostEqualMatrix(t1.tableau, t2.tableau, t1.rows, t1.columns);
}


Tableau defaultTableau(){
  // optimal solution: [0.0, 25.0, 60.0]
  float target[3] = {1.0f, 3.0f, 5.0f};
  float constraints[9] = {
    1.0f, 0.0f, 2.0f,
    3.0f, 1.0f, 0.0f,
    0.0f, 2.0f, 1.0f
  };
  float values[3] = {120.0f, 90.0f, 110.0f};
  return buildTableau(target, constraints, values, 3, 3);
}


void printTableau(Tableau tableau){
  for(int row = 0; row < tableau.rows; row++){
    for(int column = 0; column < tableau.columns; column++){
      printf("%f\t", tableau.tableau[INDEX(row, column, tableau.columns)]);
    }
    printf("\n");
  }
}
