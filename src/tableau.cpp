#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>

#include "tableau.h"


/**
 * Build a tableau with the given target function, constraints and values.
 * It adds the slack variables and formats the data for the rest of the algorithms.
 */
Tableau buildTableau(float *target, float *constraints, float *values, int varCount, int constraintCount){

	// init tableau
	int rowCount = constraintCount + 1;
	int columnCount = varCount + constraintCount + 2;

  float *tableau = (float*)calloc(rowCount * columnCount, sizeof(float)); // zeros

	// add target function
	tableau[INDEX(0, 0, columnCount)] = 1.0f;
	for (int i = 0; i < varCount; i++){
		tableau[INDEX(0, i+1, columnCount)] = -target[i];
	}

	// add constraints
	int lastColumn = columnCount - 1;
	for (int constraintRow = 0; constraintRow < constraintCount; constraintRow++){
		// calculate constraint row in the tableau
		int row = constraintRow + 1;
		// set slack variable
		int slackVariableColumn = varCount + constraintRow + 1;
		tableau[INDEX(row, slackVariableColumn, columnCount)] = 1.0f;
		// set value
		tableau[INDEX(row, lastColumn, columnCount)] = values[constraintRow];
		// set constraint coeficients
		for (int constraintCol = 0; constraintCol < varCount; constraintCol++){
			tableau[INDEX(row, constraintCol+1, columnCount)] = constraints[INDEX(constraintRow, constraintCol, varCount)];
		}
	}

	// build struct
	Tableau fullTableau;
	fullTableau.tableau = tableau;
	fullTableau.rows = rowCount;
	fullTableau.columns = columnCount;
	fullTableau.varCount = varCount;
	fullTableau.constraintCount = constraintCount;

	return fullTableau;
}


/**
 * Free the alocated memory for the given tableau.
 * Assumes the given tableau was created by `buildTableau`.
 */
void freeTableau(Tableau tableau){
	free(tableau.tableau);
}


/**
 * Check if the given tableau is solved.
 */
bool _isSolved(Tableau tableau){
	for (int i = 1; i < tableau.columns - 1; i++){
		if (tableau.tableau[INDEX(0, i, tableau.columns)] < 0.0f){
			return false;
		}
	}
	return true;
}


/**
 * Perform a pivot operation on the given tableau using the given pivot row and
 * pivot column.
 * A pivot operation sets all the values of the pivot column to zero except
 * for the pivot itself, by performing simple row operations.
 */
void _pivot(Tableau tableau, int pivotRow, int pivotColumn){
	float pivotValue = tableau.tableau[INDEX(pivotRow, pivotColumn, tableau.columns)];

	for (int col = 0; col < tableau.columns; col++){
		tableau.tableau[INDEX(pivotRow, col, tableau.columns)] /= pivotValue;
	}

	for (int row = 0; row < tableau.rows; row++){
		if (row != pivotRow){
			float rowValue = tableau.tableau[INDEX(row, pivotColumn, tableau.columns)];
			for (int col = 0; col < tableau.columns; col++){
					tableau.tableau[INDEX(row, col, tableau.columns)] = tableau.tableau[INDEX(row, col, tableau.columns)] - rowValue * tableau.tableau[INDEX(pivotRow, col, tableau.columns)];
			}
		}
	}
}


/**
 * Get the row for the active variable in the given column if any.
 * A variable is active if it's the only non-zero variable in it's column.
 * Returns -1 if there's no active variable in the given column.
 */
int _activeVariableRow(Tableau tableau, int column){
	int activeVariableRow = -1;

	for (int row = 0; row < tableau.rows; row++){
		if (tableau.tableau[INDEX(row, column, tableau.columns)] != 0.0f){
			if (activeVariableRow >= 0){
				return -1;
			}
			else{
				activeVariableRow = row;
			}
		}
	}

	return activeVariableRow;
}


/**
 * Return the current solution for the tableau.
 * If the tableau is solved, then this is the optimal solution.
 */
float* _solution(Tableau tableau){
	float *solution = (float*)calloc(tableau.varCount, sizeof(float));
	int lastColumn = tableau.columns - 1;

	for (int col = 1; col < tableau.varCount + 1; col++){
		int activeVariableRow = _activeVariableRow(tableau, col);
		if (activeVariableRow != -1){
			solution[col-1] = tableau.tableau[INDEX(activeVariableRow, lastColumn, tableau.columns)] / tableau.tableau[INDEX(activeVariableRow, col, tableau.columns)];
		}
	}

	return solution;
}


/**
 * Calculate the pivot column of the given tableau.
 * The pivot column is the column where the target function row has the
 * negative value with the largest magnitude.
 * If all the values of the target function row are non-negative, then the
 * tableau is solved, and -1 is returned.
 */
int _pivotColumn(Tableau tableau){
	float minValue = 0.0f;
	int minValueIndex = -1;

	for (int i = 1; i < tableau.columns - 1; i++){
		float val = tableau.tableau[INDEX(0, i, tableau.columns)];
		if (val < minValue){
			minValue = val;
			minValueIndex = i;
		}
	}

	return minValueIndex;
}


/**
 * Calculate the pivot row for the current tableau, given the pivot column.
 * The pivot row is the row corresponding to the minumum test ratio.
 * Each test ratio is calculated as a/b where a is the value of the
 * right-most column and b is a positve value in the given pivot column,
 * excluding the row corresponding to the target function.
 */
int _pivotRow(Tableau tableau, int pivotColumn){
	int pivotRow = -1;
	float minRatio = 0;

	int lastColumnIndex = tableau.columns - 1;

	for (int row = 1; row < tableau.rows; row++){
		if (tableau.tableau[INDEX(row, pivotColumn, tableau.columns)] > 0.0f){
			float testRatio = tableau.tableau[INDEX(row, lastColumnIndex, tableau.columns)] / tableau.tableau[INDEX(row, pivotColumn, tableau.columns)];
			if (pivotRow == -1 || testRatio < minRatio){
				minRatio = testRatio;
				pivotRow = row;
			}
		}
	}

	return pivotRow;
}


/**
 * Solve the given tableau using the simplex algorithm on the CPU.
 * Returns the optimal solution, or NULL if the problem is unbounded.
 */
float* solveTableau(Tableau tableau){
	while (!_isSolved(tableau)){
		int pivotColumn = _pivotColumn(tableau);
		int pivotRow = _pivotRow(tableau, pivotColumn);

		if (pivotRow == -1){
			// the problem is unbounded
			return NULL;
		}

		_pivot(tableau, pivotRow, pivotColumn);
	}

	return _solution(tableau);
}


/**
 * Generate a random float in the 1..1000 range.
 */
float _randomNumber(){
	return (float)(1 + 1000*drand48());
}


/**
 * Build a random tableau with the given number of decision variables.
 */
Tableau randomTableau(int varCount){
	float *target = (float*) malloc(varCount * sizeof(float));
  float *constraints = (float*) malloc(varCount * varCount * sizeof(float));
	float *values = (float*) malloc(varCount * sizeof(float));

	for (int var = 0; var < varCount; var++){
		// target function
		target[var] = _randomNumber();
		// constraint
		for (int constraint = 0; constraint < varCount; constraint++){
			constraints[INDEX(var, constraint, varCount)] = _randomNumber();
		}
		// value
		values[var] = _randomNumber();
	}

	// build tableau
	Tableau tableau = buildTableau(target, constraints, values, varCount, varCount);

	// clean up
	free(target);
	free(constraints);
	free(values);

	return tableau;
}


Tableau deepCopyTableau(Tableau tableau){
	Tableau copy;
	copy.rows = tableau.rows;
	copy.columns = tableau.columns;
	copy.varCount = tableau.varCount;
	copy.constraintCount = tableau.constraintCount;

	if (tableau.tableau){
		size_t size = copy.rows * copy.columns * sizeof(float);
		copy.tableau = (float*)malloc(size);
		memcpy(copy.tableau, tableau.tableau, size);
	}

	return copy;
}
