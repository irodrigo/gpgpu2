[32mRunning simplex algorithms on 50 random tableaus, with 10000 decission variables each
[0m0% done
10% done
20% done
30% done
40% done
50% done
60% done
70% done
80% done
90% done
Appending average execution times to results/times.csv in CSV format (#VARS,AVG_CPU_TIME,AVG_GPU_TIME)
[32mSolved 50 random tableaus with 10000 decision variables each
[0m
Found 0 unbounded problems

- Total time on CPU: 402.206497 seconds
- Average time on CPU: 8.044130 seconds
- Max time on CPU: 13.383731 seconds
- Min time on CPU: 4.527312 seconds

- Total time on GPU: 86.576978 seconds
- Average time on GPU: 1.731540 seconds
- Max time on GPU: 2.739630 seconds
- Min time on GPU: 1.091006 seconds

Average speedup: 4.65
