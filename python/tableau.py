"""
Solve the linear programming problem with the Simplex method in Tableau format.
This module uses an object oriented approach.
"""

class Tableau:
    """
    The Tableau class represents the tableau used for solving an standar linear
    programming problem with the simplex method.

    The linear programming problem should be in it's standard form:

    max t = c1*x1 + c2*x2 + ... + cN*xN
    st
    a11*x1 + a12*x2 + ... + a1N*xN <= b1
    ...
    aM1*x1 + aM2*x2 + ... + aMN*xN <= bM
    x1, x2, ..., xN >= 0

    where:
        t is the target function to maximize
        x = [x1, x2, ..., xN] are the decision variables
        c = [c1, c2, ..., cN] are the coeficients of the target function
        A = [a11, a12, ..., a1N] are the coeficients of the restriction inequations
            ...
            [aM1, aM2, ..., aMN]
        b = [b1, b2, ..., bM] are the values of the restriction inequations.
    """

    def __init__(self, target, constraints = None, values = None):
        """
        Inits a tableau with the given objective function, constraints and values.

        Parameters:
        -----------
        target : list
            The list of coeficcients of the target function to maximize.

        constraints : list
            The list of contraints for the target function.
            Each constraint is a vector containing the coeficients of the
            corresponding inequation.

        values : list
            The list of values for the restriction inequations
        """
        self.target = target
        self.constraints = constraints if constraints != None else []
        self.values = values if values != None else []
        self.tableau = []


    def addConstraint(self, contraint, value):
        """
        Adds the given constraint to the tableau.
        The given constraint is a row of the matrix A and the given value is the
        corresponding value of the vector b.
        """
        self.constraints.append(contraint)
        self.values.append(value)


    def solve(self):
        """
        Try to solve the tableau and return the optimal solution to the target
        function given the constraints.
        Return the optimal solution or None if the problem is unbounded.
        """
        self._build()

        while not self._isSolved():
            pivotColumn = self._pivotColumn()
            pivotRow = self._pivotRow(pivotColumn)
            if pivotRow == None:
                # problem is unbounded
                return None
            self._pivot(pivotRow, pivotColumn)

        return self._solution()


    def _build(self):
        """
        Build the full tableau for the current target function and constraints.
        A slack variable is added for each constraint.
        """
        constraintLen = len(self.constraints)
        self.tableau = []
        self.tableau.append([1] + [-t for t in self.target] + [0]*constraintLen + [0])
        for i in range(constraintLen):
            ident = [0] * constraintLen
            ident[i] = 1
            self.tableau.append([0] + self.constraints[i] + ident + [self.values[i]])


    def _isSolved(self):
        """
        Private method to check if the tableau is already solved.
        Precondition: The tableau has to be built already.
        """
        return min(self.tableau[0][1:-1]) >= 0


    def _pivot(self, pivotRow, pivotColumn):
        """
        Perform a pivot operation on the tableau using the given pivot row and
        pivot column.

        A pivot operation sets all the values of the pivot column to zero except
        for the pivot itself, by performing simple row operations.

        Precondition: The tableau has to be built already.
        """
        pivotValue = self.tableau[pivotRow][pivotColumn]

        for col in range(self._columns()):
            self.tableau[pivotRow][col] /= pivotValue

        for row in range(self._rows()):
            if row != pivotRow:
                rowValue = self.tableau[row][pivotColumn]
                for col in range(self._columns()):
                    self.tableau[row][col] = self.tableau[row][col] - rowValue*self.tableau[pivotRow][col]


    def _solution(self):
        """
        Return the optimal solution for the tableau.
        Precondition: The tableau has to be solved already.
        """
        solution = [0 for x in range(self._columns() - 1)]
        lastColumn = self._columns() - 1

        for col in range(self._columns() - 1):
            activeVariableRow = self._activeVariableRow(col)
            if activeVariableRow != None:
                solution[col] = self.tableau[activeVariableRow][lastColumn] / self.tableau[activeVariableRow][col]

        return solution[1:(len(self.target)+1)]


    def _activeVariableRow(self, column):
        """
        Get the row for the active variable in the given column if any.
        A variable is active if it's the only non-zero variable in it's column.
        """
        activeVariableRow = -1
        for row in range(self._rows()):
            if self.tableau[row][column] != 0.0:
                if activeVariableRow >= 0:
                    return None
                else:
                    activeVariableRow = row

        if activeVariableRow >= 0:
            return activeVariableRow
        else:
            return None


    def _columns(self):
        """
        Private method to get the number of columns in the tableau.
        Precondition: The tableau has to be built already.
        """
        return len(self.tableau[0])


    def _rows(self):
        """
        Private method to get the number of rows in the tableau.
        Precondition: The tableau has to be built already.
        """
        return len(self.tableau)


    def _pivotColumn(self):
        """
        Private method to calculate the pivot column for the current tableau.

        The pivot column is the column where the target function row has the
        negative value with the largest magnitude.
        If all the values of the target function row are non-negative, then the
        tableau is solved, and None is returned.

        Precondition: The tableau has to be built already.
        """
        minValue = 0
        minValueIndex = 0

        for i in range(1, self._columns() - 1):
            if self.tableau[0][i] < minValue:
                minValue = self.tableau[0][i]
                minValueIndex = i

        if minValueIndex == 0:
            return None
        else:
            return minValueIndex


    def _pivotRow(self, pivotColumn):
        """
        Private method to calculate the pivot row for the current tableau, given
        the pivot column.

        The pivot row is the row corresponding to the minumum test ratio.
        Each test ratio is calculated as a/b where a is the value of the
        right-most column and b is a positve value in the given pivot column,
        excluding the row corresponding to the target function.

        Precondition: The tableau has to be built already.
        """
        pivotRow = 0
        minRatio = 0

        lastColumnIndex = self._columns() - 1

        for row in range(1, self._rows()):
            if self.tableau[row][pivotColumn] > 0:
                testRatio = self.tableau[row][lastColumnIndex] / self.tableau[row][pivotColumn]
                if pivotRow == 0 or testRatio < minRatio:
                    minRatio = testRatio
                    pivotRow = row

        if pivotRow == 0:
            return None
        else:
            return pivotRow
