"""
Test suite for the simplex module.
"""

import unittest
from tableau import Tableau


class TestTableau(unittest.TestCase):
    """
    Tests for the tableau module.
    """

    def assertAlmostEqualList(self, list1, list2):
        """
        Assert two lists are almost equal as defined by assertAlmostEqual.
        """
        self.assertEqual(len(list1), len(list2))
        for i in range(len(list1)):
            self.assertAlmostEqual(list1[i], list2[i])


    def setUp(self):
        self.tableau = Tableau(target = [1.0, 3.0, 5.0])   # 375.0
        self.tableau.addConstraint([1.0, 0.0, 2.0], 120.0) # 120 <= 120 [checked]
        self.tableau.addConstraint([3.0, 1.0, 0.0],  90.0) # 25 <= 90 [checked]
        self.tableau.addConstraint([0.0, 2.0, 1.0], 110.0) # 110 <= 110 [checked]
        # solution: [0.0, 25.0, 60.0]

    def testInit(self):
        """
        Test the init method.
        """
        emptyTableau = Tableau(target = [1.0, 2.0, 3.0])

        self.assertAlmostEqualList(emptyTableau.target, [1.0, 2.0, 3.0])
        self.assertEqual(emptyTableau.constraints, [])
        self.assertEqual(emptyTableau.values, [])
        self.assertEqual(emptyTableau.tableau, [])

        target = [4.0, -1.0, 7.0]
        constraints = [
            [1.0, 4.0, 8.0],
            [0.0, 1.0, -1.0],
            [3.0, 2.0, 0.0],
        ]
        values = [10.0, 11.0, 12.0]
        fullTableau = Tableau(target, constraints, values)

        self.assertAlmostEqualList(fullTableau.target, target)
        self.assertEqual(fullTableau.constraints, constraints)
        self.assertEqual(fullTableau.values, values)
        self.assertEqual(fullTableau.tableau, [])


    def testAddConstraint(self):
        """
        Test the addConstraint method.
        """
        emptyTableau = Tableau(target = [1.0, 2.0, 3.0])

        emptyTableau.addConstraint([5.0, 4.0, 0.0], 8.0)
        self.assertEqual(len(emptyTableau.constraints), 1)
        self.assertEqual(len(emptyTableau.values), 1)
        self.assertAlmostEqualList(emptyTableau.constraints[0], [5.0, 4.0, 0.0])
        self.assertAlmostEqual(emptyTableau.values[0], 8.0)

        emptyTableau.addConstraint([-1.0, 0.0, 19.0], 27.0)
        self.assertEqual(len(emptyTableau.constraints), 2)
        self.assertEqual(len(emptyTableau.values), 2)
        self.assertAlmostEqualList(emptyTableau.constraints[1], [-1.0, 0.0, 19.0])
        self.assertAlmostEqual(emptyTableau.values[1], 27.0)


    def testSolve(self):
        """
        Test the solve method.
        """
        solution = self.tableau.solve()
        self.assertAlmostEqualList(solution, [0.0, 25.0, 60.0])


    def testBuild(self):
        """
        Test the _build method.
        """
        self.tableau._build()

        self.assertEqual(len(self.tableau.tableau), 4)
        self.assertAlmostEqualList(self.tableau.tableau[0], [1.0, -1.0, -3.0, -5.0, 0.0, 0.0, 0.0,   0.0])
        self.assertAlmostEqualList(self.tableau.tableau[1], [0.0,  1.0,  0.0,  2.0, 1.0, 0.0, 0.0, 120.0])
        self.assertAlmostEqualList(self.tableau.tableau[2], [0.0,  3.0,  1.0,  0.0, 0.0, 1.0, 0.0,  90.0])
        self.assertAlmostEqualList(self.tableau.tableau[3], [0.0,  0.0,  2.0,  1.0, 0.0, 0.0, 1.0, 110.0])


    def testIsSolved(self):
        """
        Test the _isSolved method.
        """
        self.tableau._build()
        self.assertFalse(self.tableau._isSolved())

        self.tableau.solve()
        self.assertTrue(self.tableau._isSolved())


    def testPivotColumn(self):
        """
        Test the _pivotColumn method.
        """
        self.tableau._build()
        self.assertEqual(self.tableau._pivotColumn(), 3)

        self.tableau.solve()
        self.assertEqual(self.tableau._pivotColumn(), None)


    def testPivotRow(self):
        """
        Test the _pivotRow method.
        """
        self.tableau._build()
        self.assertEqual(self.tableau._pivotRow(3), 1)


    def testPivot(self):
        """
        Test the _pivot method.
        """
        self.tableau._build()

        self.assertEqual(len(self.tableau.tableau), 4)
        self.assertAlmostEqualList(self.tableau.tableau[0], [1.0, -1.0, -3.0, -5.0, 0.0, 0.0, 0.0,   0.0])
        self.assertAlmostEqualList(self.tableau.tableau[1], [0.0,  1.0,  0.0,  2.0, 1.0, 0.0, 0.0, 120.0])
        self.assertAlmostEqualList(self.tableau.tableau[2], [0.0,  3.0,  1.0,  0.0, 0.0, 1.0, 0.0,  90.0])
        self.assertAlmostEqualList(self.tableau.tableau[3], [0.0,  0.0,  2.0,  1.0, 0.0, 0.0, 1.0, 110.0])
        self.assertFalse(self.tableau._isSolved())
        self.assertEqual(self.tableau._pivotColumn(), 3)
        self.assertEqual(self.tableau._pivotRow(3), 1)

        self.tableau._pivot(1, 3)

        self.assertAlmostEqualList(self.tableau.tableau[0], [1.0,  1.5, -3.0,  0.0,  2.5, 0.0, 0.0, 300.0]) # + 5 r1
        self.assertAlmostEqualList(self.tableau.tableau[1], [0.0,  0.5,  0.0,  1.0,  0.5, 0.0, 0.0,  60.0]) # pivot r1 / 2
        self.assertAlmostEqualList(self.tableau.tableau[2], [0.0,  3.0,  1.0,  0.0,  0.0, 1.0, 0.0,  90.0]) # ready
        self.assertAlmostEqualList(self.tableau.tableau[3], [0.0, -0.5,  2.0,  0.0, -0.5, 0.0, 1.0,  50.0]) # - r1
        self.assertFalse(self.tableau._isSolved())
        self.assertEqual(self.tableau._pivotColumn(), 2)
        self.assertEqual(self.tableau._pivotRow(2), 3)

        self.tableau._pivot(3, 2)

        self.assertAlmostEqualList(self.tableau.tableau[0], [1.0,  0.75,  0.0,  0.0,  1.75, 0.0,  1.5, 375.0]) # + 3 r3
        self.assertAlmostEqualList(self.tableau.tableau[1], [0.0,   0.5,  0.0,  1.0,   0.5, 0.0,  0.0,  60.0]) # ready
        self.assertAlmostEqualList(self.tableau.tableau[2], [0.0,  3.25,  0.0,  0.0,  0.25, 1.0, -0.5,  65.0]) # - 1 r3
        self.assertAlmostEqualList(self.tableau.tableau[3], [0.0, -0.25,  1.0,  0.0, -0.25, 0.0,  0.5,  25.0]) # pivot r3 / 2
        self.assertTrue(self.tableau._isSolved())


    def testSolution(self):
        """
        Test the _solution method.
        """
        self.tableau.solve()
        self.assertAlmostEqualList(self.tableau._solution(), [0.0, 25.0, 60.0])


    def testActiveVariableRow(self):
        """
        Test the _activeVariableRow method.
        """
        self.tableau._build()

        self.assertEqual(self.tableau._rows(), 4)
        self.assertEqual(self.tableau._columns(), 8)
        self.assertAlmostEqualList(self.tableau.tableau[0], [1.0, -1.0, -3.0, -5.0, 0.0, 0.0, 0.0,   0.0])
        self.assertAlmostEqualList(self.tableau.tableau[1], [0.0,  1.0,  0.0,  2.0, 1.0, 0.0, 0.0, 120.0])
        self.assertAlmostEqualList(self.tableau.tableau[2], [0.0,  3.0,  1.0,  0.0, 0.0, 1.0, 0.0,  90.0])
        self.assertAlmostEqualList(self.tableau.tableau[3], [0.0,  0.0,  2.0,  1.0, 0.0, 0.0, 1.0, 110.0])
        self.assertFalse(self.tableau._isSolved())

        self.assertEqual(self.tableau._activeVariableRow(0), 0)
        self.assertEqual(self.tableau._activeVariableRow(1), None)
        self.assertEqual(self.tableau._activeVariableRow(2), None)
        self.assertEqual(self.tableau._activeVariableRow(3), None)
        self.assertEqual(self.tableau._activeVariableRow(4), 1)
        self.assertEqual(self.tableau._activeVariableRow(5), 2)
        self.assertEqual(self.tableau._activeVariableRow(6), 3)
        self.assertEqual(self.tableau._activeVariableRow(7), None)

        self.tableau.solve()

        self.assertTrue(self.tableau._isSolved())
        self.assertAlmostEqualList(self.tableau.tableau[0], [1.0,  0.75,  0.0,  0.0,  1.75, 0.0,  1.5, 375.0])
        self.assertAlmostEqualList(self.tableau.tableau[1], [0.0,   0.5,  0.0,  1.0,   0.5, 0.0,  0.0,  60.0])
        self.assertAlmostEqualList(self.tableau.tableau[2], [0.0,  3.25,  0.0,  0.0,  0.25, 1.0, -0.5,  65.0])
        self.assertAlmostEqualList(self.tableau.tableau[3], [0.0, -0.25,  1.0,  0.0, -0.25, 0.0,  0.5,  25.0])

        self.assertEqual(self.tableau._activeVariableRow(0), 0)
        self.assertEqual(self.tableau._activeVariableRow(1), None)
        self.assertEqual(self.tableau._activeVariableRow(2), 3)
        self.assertEqual(self.tableau._activeVariableRow(3), 1)
        self.assertEqual(self.tableau._activeVariableRow(4), None)
        self.assertEqual(self.tableau._activeVariableRow(5), 2)
        self.assertEqual(self.tableau._activeVariableRow(6), None)
        self.assertEqual(self.tableau._activeVariableRow(7), None)



    def testColumns(self):
        """
        Test the _columns method.
        """
        self.tableau._build()
        self.assertEqual(self.tableau._columns(), 8)


    def testRows(self):
        """
        Test the _rows method.
        """
        self.tableau._build()
        self.assertEqual(self.tableau._rows(), 4)



if __name__ == '__main__':
    unittest.main()
