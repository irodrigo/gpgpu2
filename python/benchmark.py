"""
Benchmarking module for the Tableau module.
"""

from tableau import Tableau
import random
import time
import sys


DEFAULT_TABLEAU_COUNT = 100
DEFAULT_VAR_COUNT = 1000


def randomNumber():
    """
    Generate a random positive number.
    """
    return random.randrange(1, 100)


def randomTableau(varCount):
    """
    Create a random tableau with the given number of variables.
    The tableau might or might not have a solution, but it's not too dense to
    improve the odds.
    """
    target = [randomNumber() for x in range(varCount)]
    tableau = Tableau(target = target)

    for idx in range(varCount):
        constraint = [randomNumber() for x in range(varCount)]
        value = randomNumber()
        tableau.addConstraint(constraint, value)

    return tableau


def timeSolve(tableau):
    """
    Solve the given tableau and time the effort.
    Returns a tuple with the needed time in seconds and a boolean fag
    indicating if a solution was found or not.
    """
    startTime = time.time()
    solution = tableau.solve()
    endTime = time.time()
    return (endTime - startTime, solution != None)



if __name__ == '__main__':
    """
    Solve a number of random tableaus and time the efforts.
    """

    print('Usage: python3 tableau_benchmark [varCount] [tableauCount]')
    print('varCount is an optional parameter indicating the number of decission variables per random tableau')
    print('tableauCount is an optional parameter indicating the number of different random tableaus to solve')

    varCount = DEFAULT_VAR_COUNT
    tableauCount = DEFAULT_TABLEAU_COUNT

    if len(sys.argv) > 1:
        varCount = int(sys.argv[1])

    if len(sys.argv) > 2:
        tableauCount = int(sys.argv[2])

    print('')
    print('Solving %i randomly generated tableaus with %i decission variables each...' % (tableauCount, varCount))

    solutions = []
    times = []
    totalTime = 0
    successTime = 0
    successCount = 0
    minTime = 100000000.0 # TODO: fixme!
    maxTime = 0
    minSuccessTime = 100000000.0 # TODO: fixme!
    maxSuccessTime = 0
    minFailTime = 100000000.0 # TODO: fixme!
    maxFailTime = 0

    percentDone = 0

    for i in range(tableauCount):
        tableau = randomTableau(varCount)
        results = timeSolve(tableau)
        solveTime = results[0]
        success = results[1]

        times.append(solveTime)
        solutions.append(success)

        totalTime += solveTime

        if solveTime < minTime:
            minTime = solveTime

        if solveTime > maxTime:
            maxTime = solveTime

        if success:
            successCount += 1
            successTime += solveTime
            if solveTime < minSuccessTime:
                minSuccessTime = solveTime
            if solveTime > maxSuccessTime:
                maxSuccessTime = solveTime
        else:
            if solveTime < minFailTime:
                minFailTime = solveTime
            if solveTime > maxFailTime:
                maxFailTime = solveTime

        # show progress
        if i % (tableauCount / 10) == 0:
            print('%i%% done' % percentDone)
            percentDone += 10

    avgTime = totalTime / tableauCount
    avgSuccessTime = successTime / successCount if successCount > 0 else 0
    failCount = tableauCount - successCount
    avgFailTime = (totalTime - successTime) / failCount if failCount > 0 else 0
    successPercentage = successCount / tableauCount * 100

    print('Done!')
    print('')
    print('Results')
    print('-------')
    print('')
    print('Average solve time for %i variables: %f seconds' % (varCount, avgTime))
    print('Total time for solving %i tableaus: %f seconds' % (tableauCount, totalTime))
    print('')
    print('Minimum solve time for %i variables: %f seconds' % (varCount, minTime))
    print('Maximum solve time for %i variables: %f seconds' % (varCount, maxTime))
    print('')
    print('Succesfully solved %.01f %% of the randomly generated tableaus' % successPercentage)


    if failCount > 0:
        print('')
        print('Average success time for %i variables: %f seconds' % (varCount, avgSuccessTime))
        print('Minimum success time for %i variables: %f seconds' % (varCount, minSuccessTime))
        print('Maximum success time for %i variables: %f seconds' % (varCount, maxSuccessTime))
        print('')
        print('Average fail time for %i variables: %f seconds' % (varCount, avgFailTime))
        print('Minimum success time for %i variables: %f seconds' % (varCount, minFailTime))
        print('Maximum success time for %i variables: %f seconds' % (varCount, maxFailTime))
