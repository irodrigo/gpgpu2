# Taller de GPGPU

Proyecto de investigación sobre la implementación eficiente del método Simplex en CPU-GPU.

- **Autor:** Ignacio Rodrigo
- **C.I:** 4.719.891-9
- **E-Mail:** irodrigo17@gmail.com
- **Fecha:** 29 de Agosto de 2015


## 1. Objetivo

El objetivo de este trabajo es dar una implementación eficiente del método Simplex Tableau para la resolución de problemas de programación lineal, usando un sistema híbrido de CPU y GPU.

Este trabajo se basa fuertemente en los trabajos de investigación realizados por Vincent Boyer y Didier El Baz en [[1]] y [[2]].


## 2. Motivación

Los problemas de programación lineal son muy frecuentes en distintas áreas de las ciencias y de los negocios, y los algoritmos para resolver problemas densos siguen siendo computacionalmente complejos. El método simplex es uno de estos algoritmos, descripto inicialmente por G. Dantzig en 1951 [[3]] y ampliamente estudiado junto con varias variantes [[4]]. Tiene la ventaja de ser sencillo de implementar y relativamente eficiente, sin embargo su costo computacional sigue siendo muy alto para resolver problemas grandes.

En los últimos años las GPUs han evolucionado mucho, ya no son exclusivamente para procesamiento gráfico sino unidades de procesamiento general altamente paralelas con arquitecturas SIMT (Single Instruction, Multiple Threads). Esto las convierte en plataformas muy interesantes para implementar algoritmos como el Simplex, que por su naturaleza se pueden beneficiar de la gran capacidad de cómputo y de la arquitectura masivamente paralela.


## 3. El método Simplex

Un problema de programación lineal estándar se puede definir como:

$$ \max x_0 = c'x' $$

sujeto a las siguientes restricciones:

$$ A'x' \le b' $$ $$ x' \ge 0 $$

dónde:

$$ c' = (c_1, c_2, \ldots, c_n) \in \mathbf{R}^n $$

$$ A' = \left( \begin{array}{cccc}
a_{11} & a_{12} & \ldots & a_{1n} \\
a_{21} & a_{22} & \ldots & a_{2n} \\
\vdots & \vdots & \ddots & \vdots \\
a_{m1} & a_{m2} & \ldots & a_{mn} \\
\end{array} \right)  \in \mathbf{R}^{m \times n} $$

y

$$ x' = (x_1, x_2, \ldots, x_n)^T $$

$ n $ y $ m $ son el número de variables y restricciones respectivamente.

Las restricciones se pueden reescribir como igualdades agregando $ m $ nuevas variables $ x_{n + l} $ tal que:

 $$ a_{l1}x_1 + a_{l2}x_2 + \ldots + a_{ln}x_n + x_{n + l} = b_l,\ l \in \{1, 2, \ldots, m\} $$

con $ x_{n+l} \ge 0 $ y $ c_{n+l} = 0 $. Luego, la forma estándar de un problema de programación lineal se puede reescribir de la siguiente forma:

$$ \max x_0 = cx $$

sujeto a las siguientes restricciones:

$$ Ax = b $$ $$ x \ge 0 $$

dónde:

$$ c = (c', 0, \ldots, 0) \in \mathbf{R}^{n + m} $$

$$ A = (A', I_m) \in \mathbf{R}^{m \times (n+m)}$$

$ I_m $ es la matriz identidad de tamaño $ m \times m $, y $ x = (x', x_{n+1}, \ldots, x_{n+m})^T $.

El método simplex propone un algoritmo para calcular $ x $, basado en realizar una seria de operaciones de pivote sobre una matriz llamada *tableau* que se construye a partir de $ A $, $ b $ y $ c $ de la siguiente forma:

$$ tableau = \left( \begin{array}{cccc}
1 & -c & 0 \\
0 & A & b \\
\end{array} \right)  \in \mathbf{R}^{(m+1) \times (n+m+1)}$$

Para simplificar el resto de las definiciones vamos a definir las dimensiones del tableau como $ m' = m + 1 $ y $ n' = n + m + 1 $.

En cada iteración del método Simplex, intentamos reemplazar una variable básica con una no básica, de forma que el valor de la función objetivo aumente. Los pasos del algoritmo se especifican a continuación:

1. Calcular el índice $ k $ del valor negativo más pequeño de la primera fila del tableau. Si no se encuentra este valor el tableau está resuelto, sino vamos al paso 2.
2. Calcular los cocientes $ \theta_{i,k} = s_{i,n'} / s_{i,k} $ donde $ i = 1, 2, \ldots, m' $, y obtener el índice del menor cociente positivo $ r $. Si no encontramos este valor el problema es no acotado y no se puede encontrar una solución, por lo tanto el algoritmo termina. De lo contrario avanzamos al paso 3.
3. Se actualiza el tableau de la siguiente forma:
    1. La fila $ r $ del tableau se divide por el valor de $ tableau_{r,k} $
    2. El resto de las filas del tableau se actualizan de forma que $ tableau_{i,j} = tableau_{i,j} - tableau_{i,k} \times tableau_{r,j} $.


## 4. Implementación inicial

En un esfuerzo por entender la implementación secuencial de los algoritmos y las estructuras de datos involucrados en el método Simplex se realiza una primera implementación usando Python como lenguaje de programación, principalmente por su sencillez y facilidad para razonar sobre problemas matemáticos.

Se logra definir una estructura para el tableau y los diferentes pasos del método, y se logran validar todos los algoritmos con pruebas unitarias, pero se hace evidente que la performance de un lenguaje interpretado como Python no es suficiente para alcanzar el rendimiento esperado.

La implementación inicial en Python resulta muy ineficiente incluso en problemas chicos con apenas 1000 variables. Si bien hay muchas oportunidades de optimizar esta implementación usando librerías como NumPy [[5]], compiladores just-in-time como Numba [[6]] y librerías como PyCUDA [[7]] para aprovechar la aqruitectura CUDA, esto está fuera del alcance de este proyecto. De todas formas se conserva esta implementación ya que puede ser interesante optimizarla en el futuro, dado que Python es un lenguaje muy usado en la comunidad científica.


## 5. Implementación secuencial en C

Luego de ser validada, la implementación secuencial en Python fue portada a C por eficiencia y para sentar las bases de la implementación paralela en CUDA-C, incluyendo las pruebas unitarias y los módulos utilitarios usados para generar problemas aleatorios y medir el rendimiento del algoritmo.

La implementación secuencial inicial en C es mucho más eficiente que la implementación secuencial inicial en Python, unas 1600 veces más rápida según las pruebas iniciales, y permite resolver problemas más grandes en tiempos razonables (aproximadamente 2 segundos para un problema de 6000 variables).

Esta es la implementación que se va a usar como punto de referencia para comparar la performance de la implementación paralela, y también sirve como base ya que gran parte del código generado se reutiliza en la implementación paralela en CUDA-C.

En esta etapa se define el tipo de datos `Tableau` como un `struct` de C que guarda información como la cantidad de variables y restricciones del problema, a demás de la matriz o tableau propiamente dicho. Se definen también los algoritmos básicos para crear un tableau a partir de un problema, y para crear problemas aleatorios para ser usados en las pruebas de rendimiento. También se identifican y definen los pasos intermedios del algoritmo.

Todo el código de esta implementación está en la carpeta `src/`, y las esctructuras de datos y algoritmos están definidos y documentados en el archivo `tableau.h`.


## 6. Implementación híbrida CPU+GPU

Luego de tener la implementación secuencial en C funcionando correctamente, el siguiente paso es lograr una implementación paralela en CUDA-C.

Para llegar a esta implementación primero se define un esqueleto del algoritmo de alto nivel para resolver un tableau, definiendo que pasos intermedios se van a realizar en CPU y cuáles en GPU, y como se van a coordinar estos trabajos y las transferencias de datos entre las memorias de CPU y GPU. 

A partir de un uno de los artículos de referencia [[2]] se sabe que hay operaciones como encontrar el máximo o el mínimo de una fila o columna para los que la CPU es más eficiente que la GPU, por lo tanto se implementan estos pasos intermedios usando la CPU. Los pasos que requieren muchos cálculos como la actualización del tableau aplicando operaciones de pivote se implementan en GPU usando varios kernels.

El siguiente diagrama muestra dichas interacciones:

![flowchart](http://i.imgur.com/ir6wQ8t.png)


### 6.1. Implementación inicial

El objetivo principal de la implementacion inicial es lograr la correctitud por medio de tests unitarios. La eficiencia no es prioridad en este punto, ya que se va a iterar varias veces sobre esta solución para mejorar el rendimiento.

Una optimización que si se hace en este paso es el uso de memoria compartida para guardar los valores de la columna del pivote necesarios para actualizar el tableau en cada iteración, de esta forma cada bloque de threads tiene acceso rápido a estos datos y se mejora la ocupación de la GPU al reducir la latencia de acceso a los datos que tendríamos si se leyeran de memoria global.

Se intenta usar memoria compartida para guardar los valores de la fila de pivote también, pero no se consigue una mejora en el rendimiento ya que los valores de la fila están contiguos en memoria global, por lo que el acceso es coalesced.

En este paso también se optimizan los tamaños de bloque para cada kernel, determinando empíricamente los que llevan a la mayor ocupación de la GPU, y por lo tanto al mejor rendimiento.

Debido a los tamaños de bloque seleccionados para ejecutar los kernels y a detalles internos de la arquitectura CUDA, la divergencia es mínima, y por lo tanto la performance es máxima, cuando la cantidad de variables del problema original es $ n = \dot{32} - 1 $ (y por lo tanto la cantidad de columnas y filas del tableau son múltiplos de 32). Esto se debe a que cuando se ejecuta un kernel en la GPU nVIDIA, cada multiprocesador ejecuta un bloque de threads agrupándolos en conjuntos de 32 threads llamados *warps*. Si los threads que componen un warp ejecutan las mismas instrucciones la ejecución es en paralelo y el rendimiento es máximo, en cambio si los threads divergen la ejecución se serializa impactando negativamente el rendimiento. Si el tamaño del tableau no es múltiplo de 32, algunos threads no tienen datos para procesar, y su ejecución diverge del resto.


### 6.2. Optimizaciónes del código

Esta primera implementación paralela es relativamente eficiente y ya supera notablemente a la implementación secuencial en rendimiento para problemas grandes, pero todavía hay lugar para optimizarla bastante, dichas optimizaciones se realizan en varias iteraciones que se detallan a continuación.

La herramienta `nvprof` de profiling de nVIDIA es de gran utilidad para encontrar los puntos débiles y los cuellos de botella.


#### 6.2.1. Eliminación de sincronización redundante

En la primera implementación hay muchas llamadas a `cudaDeviceSynchronize()` redundantes e innecesarias para sincronizar la ejecución de los kernels y las transferencias de memoria. Se eliminan todas las llamadas innecesarias y se observa una mejora del rendimiento, si bien no es muy significativa.


#### 6.2.2. Reutilización de memoria

Los pasos intermedios del algoritmo requieren transferencias de datos entre CPU y GPU en cada iteración. En la implementación inicial la memoria necesaria para estas transferencias era reservada y liberada en cada iteración, lo que conlleva un overhead significativo. En este paso se reserva la memoria necesaria para los pasos intermedios antes de la primer iteración, y se libera al terminar. Se observa una buena mejora en el rendimiento.


#### 6.2.3. Memoria no paginada (pinned)

En uno de los artículos de referencia [[2]] los autores notan que usar memoria no paginada (page-locked o pinned) mejora significativamente el rendimiento. En este paso se utiliza memoria no paginada para todas las transferencias incluyendo las transferencias del tableau completo incial y final, y todas las transferencias intermedias. Para mantener la compatibilidad con la implementación secuencial, la memoria del tableau en CPU se registra con `cudaHostRegister()` después de ser reservada con `malloc()` en lugar de usar `cudaMallocHost()`.

Se observa una mejora en el rendimiento pero no tan significativa como se esperaba.

Para intentar sacar más ventaja de la memoria no paginada se implementan copias asíncronas con `cudaMemcpyAsync()`, pero para sacar más provecho de esto sería necesario paralelizar la ejecución de los distintos kernels, que está fuera del alcance de este proyecto. Ésta línea de trabajo es muy interesante como trabajo a futuro y hay buenas bases en trabajos de investigación existentes [[8]].


## 7. Pruebas experimentales

Para evaluar el rendimiento de la implementación en GPU y la mejora obtenida luego de aplicar las diferentes optimizaciones se utiliza un conjunto de pruebas implementadas en `benchmark.cpp`. Básicamente se crean varios tableaus aleatorios y cada uno se resuelve usando tanto la implementación secuencial en CPU como la implementación paralela en GPU, midiendo el tiempo necesario en ambos casos. Luego se presentan los tiempos promedio, máximo y mínimo de cada implementación.

Como una prueba adicional se verifica que las soluciones obtenidas por ambas implementaciones coincidan.

Para compilar el proyecto se puede usar el script `build.sh`, luego se pueden correr los tests usando el script `test.sh`, y por último se pueden ejecutar las pruebas de rendimiento usando el script `benchmark.sh`.


## 8. Resultados

Las pruebas de rendimiento consisten en generar al menos 100 problemas aleatorios y resolver cada uno en CPU y en GPU, midiendo los tiempos de ejecución en cada caso y calculando los promedios. Las pruebas son ejecutadas en una de las PCs Linux de la facultad, con las siguientes especificaciones de hardware:

- CPU - Intel(R) Core(TM) i3-2100 CPU @ 3.10GHz
- Memoria - 8 GB
- GPU - nVIDIA GeForce GTX 480 (1.5 GB)

Luego de correr todas las pruebas se obtienen los siguientes resultados:

### 8.1. Primera versión (sin optimizar)

**Tiempo de ejecución promedio por instancia en CPU y en CPU:**

| Variables |   CPU   |  GPU  | Speedup |
|:---------:|:-------:|:-----:|:-------:|
| 500       | 0.002   | 0.007 | 0.335   |
| 1000      | 0.021   | 0.014 | 1.534   |
| 2000      | 0.115   | 0.042 | 2.744   |
| 4000      | 0.650   | 0.184 | 3.527   |
| 6000      | 2.040   | 0.516 | 3.952   |
| 8000      | 4.591   | 1.093 | 4.202   |
| 10000     | 7.985   | 1.863 | 4.287   |
| 12000     | 13.828  | 3.117 | 4.436   |

![time-chart-1.0](http://i.imgur.com/8dKg1hf.png)

### 8.1.2. Última versión con todas las optimizaciones

**Tiempo de ejecución promedio por instancia en CPU y en CPU:**

| Variables |   CPU   |  GPU  | Speedup |
|:---------:|:-------:|:-----:|:-------:|
| 500       | 0.003   | 0.003 | 0.910   |
| 1000      | 0.021   | 0.008 | 2.690   |
| 2000      | 0.115   | 0.032 | 3.582   |
| 4000      | 0.651   | 0.160 | 4.065   |
| 6000      | 2.039   | 0.466 | 4.372   |
| 8000      | 4.560   | 1.006 | 4.533   |
| 10000     | 8.044   | 1.732 | 4.646   |
| 12000     | 13.827  | 2.925 | 4.728   |


![time-chart-1.5](http://i.imgur.com/DZdZQNF.png)

![speedup-chart-1.5](http://i.imgur.com/JuIt7Zr.png)


Los resultados detallados de las pruebas incluyendo tablas y gráficas se pueden encontrar en las carpetas `results-1.0` y `results-1.5` para la version inicial y optimizada respectivamente.

A continuación se detallan los resultados obtenidos en el trabajo de referencia [[2]], de forma de poner en perspectiva los resultados obtenidos en éste trabajo y tener un punto de comparación:

**Hardware:**

- CPU - Intel Xeon 3.0 GHz
- Memoria - Desconocida
- GPU - NVIDIA GTX 260 GPU

**Tiempo de ejecución promedio por instancia en CPU y en GPU:**

| Variables |   CPU   |  GPU   |
|:---------:|:-------:|:------:|
| 500       | 0.116   | 0.044  |
| 1000      | 2.928   | 0.337  |
| 2000      | 52.461  | 4.430  |
| 4000      | 751.775 | 59.634 |

Nótese que si bien el hardware utilizado en las pruebas experimentales de este trabajo es más un poco mas potente, los tiempos absolutos logrados en este trabajo son mucho menores que los anteriores. Si comparamos los resultados obtenidos en problemas de 4000 variables, vemos una mejora de 15x usando la implementación en GPU, y 1155x usando la implementación en CPU. Lamentablemente no tenemos el código del trabajo anterior para correr las pruebas en nuestro hardware y poder hacer una comparación más justa, pero a simple vista parece que la mejora observada en rendimiento supera el aumento de la capacidad de procesamiento.

En un intento por comparar la capacidad de procesamiento del hardware utilizado en ambos trabajos, vamos a comparar los rendimientos aproximados en GFLOPS para las distintas CPUs y GPUs. Hay pruebas que reportan que la CPU Intel Xeon 3.0 Ghz alcanza rendimientos de 38 GFLOPS, mientras que el Intel i3-2100 alcanza rendimientos de 48 GFLOPS, esto representa una mejora de apenas 1.5x en el rendimiento del hardware que contrasta con la mejora de 1156x observada en el rendimiento del algoritmo en CPU. La GPU nVIDIA GTX 260 reporta 847 GFLOPS teóricos, mientras que la nVIDIA GTX 480 reporta 1345 GFLOPS teóricos, una mejora de 1.6x aproximadamente, que de nuevo contrasta con la mejora de 15x observada en el rendimiento del algoritmo en GPU. Esta comparación del hardware no es de ninguna forma precisa ni exhaustiva, pero puede servir para tener un punto de comparación aproximado entre los resultados obtenidos aquí y en el trabajo de referencia [[2]].

Cabe destacar también que en éste trabajo se logran resolver problemas de hasta 13500 variables, mientras que en el trabajo de referencia se reportan resultados con problemas de hasta 4000 variables.


## 9. Conclusiones

Se concluye que la implementación paralela usando la tecnología CUDA es entre cuatro y cinco veces más eficiente que la implementación secuencial, dependiendo del tamaño del problema, ya que muestra su verdadero potencial en problemas grandes con más de 1000 variables. Esto nos permite resolver problemas más grandes en tiempos razonables. Por otro lado, el algoritmo no es eficiente en problemas pequeños de menos de 1000 variables, siendo preferible la implementación secuencial para resolver estos problemas.

Si bien no se pueden comparar los resultados directamente por las diferencias de hardware, la evidencia parece indicar que se logra una mejora significativa de rendimiento con respecto a la implementación de referencia realizada en [[2]].

Se confirma la importancia de conocer la arquitectura de las CUDA y realizar las optimizaciones correspondientes para sacar el máximo provecho del hardware disponible.


## 10. Limitaciones

Hay ciertas limitaciones inherentes al método Simplex en si, y algunas a la implementación misma:

- El algoritmo necesita mucha memoria para guardar el tableau completo, aproximadamente $ 8 \times N^2 $ bytes donde $ N $ es el número de variables del problema a resolver. Por lo tanto para un problema de 10000 variables se necesitan unos 763 MB solo para el tableau. Esto limita mucho el tamaño de problemas que se pueden resolver, en las PCs de prueba se pudieron resolver problemas de hasta 13500 antes de alcanzar el límite de memoria de la GPU de 1.5 GB. Hay versiones conocidas del algoritmo como la utilizada en [[2]] que usan menos memoria, y podrían aumentar considerablemente el tamaño de los problemas que se pueden resolver. La reducción de memoria se basa principalmente en reorganizar la matriz $ A $ aprovechando la distinción entre variables básicas y no básicas.

- El profiler revela que la ocupación de la GPU no es óptima, esto se debe principalmente a 2 factores:
  1. Hay muchas operaciones que se realizan secuencialmente en CPU antes de poder ejecutar los kernels.
  2. El kernel de pivot que es el que consume casi la totalidad del tiempo de GPU no logra una ocupación completa.

  Esto se puede mejorar paralelizando la ejecución de los kernels y las transferencias a memoria con CUDA streams.

- El algoritmo no es eficiente para problemas pequeños, con menos de 500 variables es incluso más eficiente la implementación secuencial, y sólo demuestra su potencial en problemas de más de 1000 variables. Esto se debe al overhead de copiar los datos a GPU, ejecutar y sincronizar los kernels.


## 11. Trabajo futuro

Se identifican varias oportunidades de trabajo a futuro:

- Transferencias y procesamiento asíncrono de distintas partes de la matriz usando CUDA streams para mejorar la ocupación de la GPU. Es necesario particionar el tableau de modo que los diferentes streams puedan trabajar en paralelo, y sincronizarlos adecuadamente.
- Utilizar más de una GPU, basándose en trabajos existentes [[9]].
- Construcción de tableau con variables adicionales para que el numero siempre sea múltiplo de 32, de modo de minimizar la divergencia y maximizar la ocupación. A modo de ejemplo el speedup promedio aumenta de 4.73x a 5.42x al resolver un problema con 12031 variables en lugar de 12000.
- Utilizar otra versión del algoritmo con menor uso de memoria como el método Simplex Revisado [[10]] para poder resolver problemas mas grandes.
- Optimizar la implementación original en Python.


## 12. Referencias

[1]: https://drive.google.com/open?id=0BxQYEalXLJXbT2NJRnlGU3VsdE0
[2]: https://drive.google.com/open?id=0BxQYEalXLJXbT2NJRnlGU3VsdE0
[3]: https://drive.google.com/open?id=0BxQYEalXLJXbT2NJRnlGU3VsdE0
[4]: http://books.google.com.uy/books?id=zEzW5mhppB8C&lpg=PA1&ots=SmY-whQePs&dq=Theory%20of%20integer%20and%20linear%20programming&lr&hl=es&pg=PA1#v=onepage&q&f=false
[5]: http://www.numpy.org
[6]: http://numba.pydata.org
[7]: http://documen.tician.de/pycuda/
[8]: http://homepages.laas.fr/elbaz/4538a179.pdf
[9]: http://spc.unige.ch/lib/exe/fetch.php?media=pub:sgpu_europar2011.pdf
[10]: http://www.idi.ntnu.no/~elster/pubs/ipdps09-lin-prog.pdf

1. V. Boyer, D. El Baz, Recent Advances on GPU Computing in Operations Research, IEEE 27th International Symposium on Parallel & Distributed Processing Workshops and PhD Forum, 2013
2. M. Lalami, V. Boyer, and D. El Baz, “Efficient implementation of the simplex method on a CPU-GPU system,” in 25th IEEE International Parallel and Distributed Processing Symposium, Workshops and Phd Forum (IPDPSW), Workshop PCO’11, may 2011, pp. 1999 –2006.
3. G. Dantzig, “Maximization of a linear function of variables subject to linear inequalities,” in Activity Analysis of Produc- tion and Allocation. Wiley and Chapman-Hall, 1951, pp. 339–347.
4. A. Schrijver, Theory of integer and linear programming. Wiley, Chichester, 1986."
5. Sitio oficial de la librería NumPy: http://www.numpy.org
6. Sitio oficial de la librería Numba: http://numba.pydata.org
7. Sitio oficial de la librería PyCUDA: http://documen.tician.de/pycuda/
8. M. Lalami, D. El Baz, and V. Boyer, “Multi GPU implementation of the simplex algorithm,” in 13th IEEE International Conference on High Performance Computing and Communications (HPCC 2011), sept. 2011, pp. 179 –186.
9. X. Meyer, P. Albuquerque, and B. Chopard, “A multi-GPU implementation and performance model for the standard simplex method,” in Euro-Par 2011, 2011.
10. D. Spampinato and A.Elster, “Linear optimization on modern GPUs,” in 2009 IEEE International Parallel and Distributed Processing Symposium, IPDPS 2009, may 2009, pp. 1 –8.
