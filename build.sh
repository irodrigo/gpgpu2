echo "Building C++ code..."
gcc -O3 -c src/tableau.cpp src/timing.cpp src/testing.cpp

echo "Building CUDA-C code..."
nvcc -c -arch=compute_20 -code=sm_20 -Xptxas -v -w src/tableauGPU.cu
nvcc src/tests.cpp -O3 -arch=compute_20 -code=sm_20 -w -Xptxas -v -o bin/tests tableauGPU.o tableau.o timing.o testing.o
nvcc src/benchmark.cpp -O3 -arch=compute_20 -code=sm_20 -w -Xptxas -v -o bin/benchmark tableauGPU.o tableau.o timing.o testing.o
