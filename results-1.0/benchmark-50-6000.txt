[32mRunning simplex algorithms on 50 random tableaus, with 6000 decission variables each
[0m0% done
10% done
20% done
30% done
40% done
50% done
60% done
70% done
80% done
90% done
[32mSolved 50 random tableaus with 6000 decision variables each
[0m
Found 0 unbounded problems

- Total time on CPU: 102.010683 seconds
- Average time on CPU: 2.040214 seconds
- Max time on CPU: 3.613700 seconds
- Min time on CPU: 0.948045 seconds

- Total time on GPU: 25.813334 seconds
- Average time on GPU: 0.516267 seconds
- Max time on GPU: 0.820048 seconds
- Min time on GPU: 0.305312 seconds

Average speedup: 3.95
