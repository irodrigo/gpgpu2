# clean, build, test, benchmark

sh clean.sh

echo ""
sh build.sh

echo ""
sh 'test.sh'

echo ""
sh benchmark.sh
